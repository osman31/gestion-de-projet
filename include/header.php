<?php
require "../connection.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title><?= $page ?></title>
</head>
<body>
 <header>
     <div class="logo">
        <img src="./logo.png" alt="Simplon logo" >
        <h1>Gestion de project</h1>
     </div>
 </header>  

